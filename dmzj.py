import requests
import json

def simulate_network_request():
    # 定义请求的URL
    url = "https://nnv3api.idmzj.com/task/sign"

    # 定义请求参数
    params = {
        "app_channel": "101",
        "channel": "ios",
        "imei": "00000000-0000-0000-0000-000000000000",
        "iosId": "b73105f170d4410b8b934243b9bdef82",
        "terminal_model": "iPhone10%2C2",
        "timestamp": "1703556473",
        "uid": "25342526",
        "version": "4.7.10"
    }

    # 定义请求头
    headers = {
        "authority": "nnv3api.idmzj.com",
        "cookie": "love=68d5c6e6b0d16b24e47e1c913805af30; my=25342526|petermlf|peterpantc@126.com|8111abf7d75ab13a41df598711a4316a|814915dfd4bd9475493e22c8f5ca6a6a",
        "accept": "*/*",
        "user-agent": "",
        "accept-language": "zh-Hans-CN;q=1.0, en-CN;q=0.9",
        "accept-encoding": "br;q=1.0, gzip;q=0.9, deflate;q=0.8"
    }

    try:
        # 发送 GET 请求
        response = requests.get(url, params=params, headers=headers)

        # 检查响应是否成功
        if response.status_code == 200:
            # 解析 JSON 响应体
            json_response = response.json()

            # 打印响应体内容
            print("Response Code:", json_response["code"])
            print("Response Message:", json_response["msg"])
        else:
            print("Request failed with status code:", response.status_code)

    except Exception as e:
        print("An error occurred:", str(e))

if __name__ == "__main__":
    # 调用函数模拟网络请求
    simulate_network_request()
